package com.ib.asanarebelchallenge.domain.interactor.details

import com.ib.asanarebelchallenge.data.models.GithubRepositoryDetailsModel
import io.reactivex.Single

interface RepositoryDetailsInteractor {

    fun getRepositoryDetailsWithSubscribersByPage(
        owner: String,
        name: String
    ): Single<GithubRepositoryDetailsModel>
}