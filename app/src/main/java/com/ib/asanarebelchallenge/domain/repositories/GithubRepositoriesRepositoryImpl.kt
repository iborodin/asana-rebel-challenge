package com.ib.asanarebelchallenge.domain.repositories

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.ib.asanarebelchallenge.data.models.GithubRepositoryDetailsModel
import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import com.ib.asanarebelchallenge.data.models.GithubUserModel
import com.ib.asanarebelchallenge.data.models.GraphqlPageInfo
import fragment.RepositoryDetailsFragment
import fragment.RepositoryFragment
import fragment.UserFragment
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class GithubRepositoriesRepositoryImpl @Inject constructor(
    private val apolloClient: ApolloClient
) : GithubRepositoriesRepository {

    init {
        Timber.v("init GithubRepositoriesRepositoryImpl")
    }

    override fun searchRepositoriesByPage(
        searchQuery: String,
        limit: Int,
        after: String?
    ): Single<Pair<List<GithubRepositoryModel>, GraphqlPageInfo?>> {
        return SearchRepositoriesQuery
            .builder()
            .query(searchQuery)
            .limit(limit)
            .after(after)
            .build()
            .let { apolloClient.query(it) }
            .let { Rx2Apollo.from(it) }
            .subscribeOn(Schedulers.io())
            .map {
                val list = it.data()?.search()?.nodes()?.mapNotNull {
                    it.fragments().repositoryFragment()?.let { mapQueryRepositoryFragment(it) }
                } ?: emptyList()
                val pageInfo =
                    it.data()?.search()?.pageInfo()?.let { GraphqlPageInfo(it.hasNextPage(), it.endCursor()) }
                list to pageInfo
            }
            .firstOrError()
    }

    private fun mapQueryRepositoryFragment(repositoryFragment: RepositoryFragment): GithubRepositoryModel {
        return GithubRepositoryModel(
            id = repositoryFragment.id(),
            name = repositoryFragment.name(),
            description = repositoryFragment.description().orEmpty(),
            forksNumber = repositoryFragment.forks().totalCount(),
            owner = repositoryFragment.owner().let { GithubUserModel(it.login(), it.avatarUrl()) }
        )
    }

    override fun getRepositoryDetailsWithSubscribersByPage(
        owner: String,
        name: String,
        watchersLimit: Int,
        watchersAfter: String?
    ): Single<Pair<GithubRepositoryDetailsModel, GraphqlPageInfo?>> {
        return GetRepositoryInfoQuery
            .builder()
            .owner(owner)
            .name(name)
            .watchersLimit(watchersLimit)
            .watchersAfter(watchersAfter)
            .build()
            .let { apolloClient.query(it) }
            .let { Rx2Apollo.from(it) }
            .subscribeOn(Schedulers.io())
            .map {
                val details = it.data()?.repository()?.let {
                    mapQueryRepository(it.fragments().repositoryDetailsFragment())
                } ?: initEmptyRepositoryDetailsModel(name)
                val watchersPageInfo =
                    it.data()?.repository()?.fragments()?.repositoryDetailsFragment()?.watchers()?.pageInfo()?.let {
                        GraphqlPageInfo(it.hasNextPage(), it.endCursor())
                    }
                details to watchersPageInfo
            }
            .firstOrError()
    }

    private fun mapQueryRepository(repositoryInfo: RepositoryDetailsFragment): GithubRepositoryDetailsModel {
        return GithubRepositoryDetailsModel(
            name = repositoryInfo.name(),
            watchersCount = repositoryInfo.watchers().totalCount(),
            watchers = repositoryInfo.watchers().nodes()?.mapNotNull {
                mapQueryWatcher(it.fragments().userFragment())
            } ?: emptyList()
        )
    }

    private fun mapQueryWatcher(user: UserFragment): GithubUserModel {
        return GithubUserModel(
            login = user.login(),
            avatarUrl = user.avatarUrl()
        )
    }

    private fun initEmptyRepositoryDetailsModel(name: String): GithubRepositoryDetailsModel {
        return GithubRepositoryDetailsModel(
            name = name,
            watchersCount = 0,
            watchers = emptyList()
        )
    }
}