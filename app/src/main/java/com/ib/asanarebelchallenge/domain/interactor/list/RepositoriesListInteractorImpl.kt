package com.ib.asanarebelchallenge.domain.interactor.list

import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import com.ib.asanarebelchallenge.domain.repositories.GithubRepositoriesRepository
import io.reactivex.Single
import javax.inject.Inject

class RepositoriesListInteractorImpl @Inject constructor(
    private val repository: GithubRepositoriesRepository
) : RepositoriesListInteractor {

    private var lastSearchQuery: String = ""
    private var hasNextPage: Boolean = true
    private var endCursor: String? = null

    override fun searchRepositoriesByPage(
        searchQuery: String
    ): Single<List<GithubRepositoryModel>> {

        if (searchQuery != lastSearchQuery) {
            lastSearchQuery = searchQuery
            hasNextPage = true
            endCursor = null
        }

        return if (hasNextPage.not()) {
            Single.just(emptyList())
        } else {
            repository.searchRepositoriesByPage(
                searchQuery = searchQuery,
                after = endCursor
            )
                .doOnSuccess { (_, pageInfo) ->
                    hasNextPage = pageInfo?.hasNextPage ?: false
                    endCursor = pageInfo?.endCursor
                }
                .map { it.first }
        }
    }
}