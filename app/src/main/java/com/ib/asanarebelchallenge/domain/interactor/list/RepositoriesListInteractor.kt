package com.ib.asanarebelchallenge.domain.interactor.list

import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import io.reactivex.Single

interface RepositoriesListInteractor {

    fun searchRepositoriesByPage(
        searchQuery: String
    ): Single<List<GithubRepositoryModel>>

}