package com.ib.asanarebelchallenge.domain.interactor.details

import com.ib.asanarebelchallenge.data.models.GithubRepositoryDetailsModel
import com.ib.asanarebelchallenge.domain.repositories.GithubRepositoriesRepository
import io.reactivex.Single
import javax.inject.Inject

class RepositoryDetailsInteractorImpl @Inject constructor(
    private val repository: GithubRepositoriesRepository
) : RepositoryDetailsInteractor {

    private var lastOwnerQuery: String = ""
    private var lastNameQuery: String = ""

    private var watchersHasNextPage: Boolean = true
    private var watchersEndCursor: String? = null

    override fun getRepositoryDetailsWithSubscribersByPage(
        owner: String,
        name: String
    ): Single<GithubRepositoryDetailsModel> {

        if (repositoryIsTheSame(owner, name).not()) {
            lastOwnerQuery = owner
            lastNameQuery = name
            watchersHasNextPage = true
            watchersEndCursor = null
        }

        return repository.getRepositoryDetailsWithSubscribersByPage(
            owner = owner,
            name = name,
            watchersAfter = watchersEndCursor
        )
            .doOnSuccess { (_, pageInfo) ->
                watchersHasNextPage = pageInfo?.hasNextPage ?: false
                watchersEndCursor = pageInfo?.endCursor
            }
            .map { it.first }
    }

    private fun repositoryIsTheSame(owner: String, name: String) = lastOwnerQuery == owner && lastNameQuery == name

}