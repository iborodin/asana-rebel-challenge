package com.ib.asanarebelchallenge.domain.repositories

import com.ib.asanarebelchallenge.data.models.GithubRepositoryDetailsModel
import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import com.ib.asanarebelchallenge.data.models.GraphqlPageInfo
import io.reactivex.Single

interface GithubRepositoriesRepository {

    fun searchRepositoriesByPage(
        searchQuery: String,
        limit: Int = PAGE_SIZE,
        after: String? = null
    ): Single<Pair<List<GithubRepositoryModel>, GraphqlPageInfo?>>

    fun getRepositoryDetailsWithSubscribersByPage(
        owner: String,
        name: String,
        watchersLimit: Int = PAGE_SIZE,
        watchersAfter: String? = null
    ): Single<Pair<GithubRepositoryDetailsModel, GraphqlPageInfo?>>

    companion object {
        private const val PAGE_SIZE = 30
    }
}