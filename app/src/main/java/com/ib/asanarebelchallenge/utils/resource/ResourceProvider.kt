package com.ib.asanarebelchallenge.utils.resource

interface ResourceProvider {

    fun getString(id: Int): String

    fun getPluralString(id: Int, quantity: Int, vararg formatArgs: Any): String
}