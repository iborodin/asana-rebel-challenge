package com.ib.asanarebelchallenge.utils

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun <T : Fragment> T.withArguments(action: Bundle.() -> Unit): T {
    arguments = Bundle().apply(action)
    return this
}

fun ViewGroup.inflateLayout(@LayoutRes res: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(this.context).inflate(res, this, attachToRoot)

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }