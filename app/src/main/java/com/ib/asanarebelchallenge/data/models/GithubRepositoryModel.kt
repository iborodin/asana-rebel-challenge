package com.ib.asanarebelchallenge.data.models

data class GithubRepositoryModel(
    val id: String,
    val name: String,
    val description: String,
    val forksNumber: Int,
    val owner: GithubUserModel
)