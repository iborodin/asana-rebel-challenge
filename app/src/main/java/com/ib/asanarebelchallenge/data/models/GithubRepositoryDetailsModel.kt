package com.ib.asanarebelchallenge.data.models

data class GithubRepositoryDetailsModel(
    val name: String,
    val watchersCount: Int,
    val watchers: List<GithubUserModel>
)