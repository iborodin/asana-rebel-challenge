package com.ib.asanarebelchallenge.data.models

data class GithubUserModel(
    val login: String,
    val avatarUrl: String
)