package com.ib.asanarebelchallenge.data.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthHeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.request()
            .newBuilder()
            .header(AUTH_HEADER_NAME, "Bearer 47be345d8a33760f77a666e27d60391d3333fa75")
            .build()
            .let { chain.proceed(it) }
    }

    companion object {
        private const val AUTH_HEADER_NAME = "Authorization"
    }
}