package com.ib.asanarebelchallenge.ui

import android.support.v7.util.DiffUtil

class RepositoryUiDiffCallback(
    private val oldItems: List<RepositoryUiModel>,
    private val newItems: List<RepositoryUiModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return (oldItem is LoadingListItem && newItem is LoadingListItem) ||
                (oldItem is ErrorListItem && newItem is ErrorListItem) ||
                (oldItem is RepositoryListItem && newItem is RepositoryListItem &&
                        oldItem.repository.id == newItem.repository.id)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}