package com.ib.asanarebelchallenge.ui.details.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.data.models.GithubUserModel
import com.ib.asanarebelchallenge.di.details.RepositoryName
import com.ib.asanarebelchallenge.di.details.RepositoryOwner
import com.ib.asanarebelchallenge.domain.interactor.details.RepositoryDetailsInteractor
import com.ib.asanarebelchallenge.ui.details.ErrorListItem
import com.ib.asanarebelchallenge.ui.details.LoadingListItem
import com.ib.asanarebelchallenge.ui.details.UserListItem
import com.ib.asanarebelchallenge.ui.details.UserUiModel
import com.ib.asanarebelchallenge.utils.resource.ResourceProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class RepositoryDetailsPresenter @Inject constructor(
    private val interactor: RepositoryDetailsInteractor,
    private val resourceProvider: ResourceProvider,
    @RepositoryOwner private val repositoryOwner: String,
    @RepositoryName private val repositoryName: String
) : MvpPresenter<RepositoryDetailsView>() {

    private val subscriptions = CompositeDisposable()

    private val currentWatchers = mutableListOf<GithubUserModel>()
    private var watchersHasNextPage: Boolean = true
    private var isLoading: Boolean = false

    override fun onFirstViewAttach() {
        viewState.showTitle(repositoryName)
        requestNextWatchers()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }

    fun requestNextWatchers() {
        if (isLoading || watchersHasNextPage.not()) return

        interactor.getRepositoryDetailsWithSubscribersByPage(repositoryOwner, repositoryName)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                viewState.showEmptyListMessage(false)
                showUsersLoading()
                isLoading = true
            }
            .doFinally { isLoading = false }
            .doOnSuccess { watchersHasNextPage = it.watchers.isNotEmpty() }
            .subscribe(
                { repositoryDetails ->
                    viewState.showSubtitle(
                        resourceProvider.getPluralString(
                            R.plurals.repository_details_toolbar_subtitle,
                            repositoryDetails.watchersCount,
                            repositoryDetails.watchersCount
                        )
                    )

                    currentWatchers.addAll(repositoryDetails.watchers)

                    viewState.showEmptyListMessage(currentWatchers.isEmpty())
                    viewState.showUsersList(currentWatchers.isNotEmpty())

                    currentWatchers
                        .map { UserListItem(it) }
                        .let(viewState::updateUserListItems)
                },
                {
                    Timber.e(it)
                    showUsersError()
                }
            )
            .let(subscriptions::add)
    }

    private fun showUsersLoading() {
        currentWatchers
            .map { UserListItem(it) }
            .toMutableList<UserUiModel>()
            .apply { add(LoadingListItem) }
            .let(viewState::updateUserListItems)
    }

    private fun showUsersError() {
        currentWatchers
            .map { UserListItem(it) }
            .toMutableList<UserUiModel>()
            .apply { add(ErrorListItem) }
            .let(viewState::updateUserListItems)
    }
}