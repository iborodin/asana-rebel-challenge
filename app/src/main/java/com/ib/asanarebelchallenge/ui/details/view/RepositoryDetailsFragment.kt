package com.ib.asanarebelchallenge.ui.details.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.data.models.GithubUserModel
import com.ib.asanarebelchallenge.ui.AppNavigator
import com.ib.asanarebelchallenge.ui.common.PaginationScrollListener
import com.ib.asanarebelchallenge.ui.details.UserUiModel
import com.ib.asanarebelchallenge.ui.details.presenter.RepositoryDetailsPresenter
import com.ib.asanarebelchallenge.ui.details.presenter.RepositoryDetailsView
import com.ib.asanarebelchallenge.utils.isVisible
import com.ib.asanarebelchallenge.utils.withArguments
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_repository_details.*
import javax.inject.Inject

class RepositoryDetailsFragment : MvpAppCompatFragment(), RepositoryDetailsView, UsersAdapter.OnClickListener {

    val repositoryOwner: String
        get() = arguments?.getString(KEY_REPOSITORY_OWNER)
            ?: error("Parameter KEY_REPOSITORY_OWNER is required for this fragment")

    val repositoryName: String
        get() = arguments?.getString(KEY_REPOSITORY_NAME)
            ?: error("Parameter KEY_REPOSITORY_NAME is required for this fragment")

    private lateinit var usersAdapter: UsersAdapter

    @Inject
    @InjectPresenter
    lateinit var presenter: RepositoryDetailsPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repository_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initList()
    }

    private fun initToolbar() {
        with(toolbar) {
            setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            setNavigationOnClickListener {
                activity
                    ?.let { it as? AppNavigator }
                    ?.let { it.onBack() }
            }
        }
    }

    private fun initList() {
        usersAdapter = UsersAdapter(this)
        val linearLayoutManager = LinearLayoutManager(context)
        watchersList.apply {
            adapter = usersAdapter
            layoutManager = linearLayoutManager
            setHasFixedSize(true)

            addOnScrollListener(
                PaginationScrollListener(
                    linearLayoutManager,
                    presenter::requestNextWatchers
                )
            )
        }
    }

    override fun showTitle(title: String) {
        toolbar.title = title
    }

    override fun showSubtitle(subtitle: String) {
        toolbar.subtitle = subtitle
    }

    override fun showEmptyListMessage(show: Boolean) {
        emptyListMessage.isVisible = show
    }

    override fun showUsersList(show: Boolean) {
        watchersList.isVisible = true
    }

    override fun updateUserListItems(users: List<UserUiModel>) {
        usersAdapter.updateItems(users)
    }

    override fun onRetryClick() {
        presenter.requestNextWatchers()
    }

    companion object {

        private const val KEY_REPOSITORY_OWNER = "KEY_REPOSITORY_OWNER"
        private const val KEY_REPOSITORY_NAME = "KEY_REPOSITORY_NAME"

        fun newInstance(repositoryOwner: String, repositoryName: String): RepositoryDetailsFragment {
            return RepositoryDetailsFragment().withArguments {
                putString(KEY_REPOSITORY_OWNER, repositoryOwner)
                putString(KEY_REPOSITORY_NAME, repositoryName)
            }
        }
    }
}