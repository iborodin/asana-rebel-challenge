package com.ib.asanarebelchallenge.ui.list.presenter

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ib.asanarebelchallenge.ui.RepositoryUiModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface RepositoriesListView : MvpView {

    fun showEmptyResultMessage(show: Boolean)

    fun showRepositoriesList(show: Boolean)

    fun updateRepositoriesListItems(repositories: List<RepositoryUiModel>)

}