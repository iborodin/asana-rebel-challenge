package com.ib.asanarebelchallenge.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.ui.details.view.RepositoryDetailsFragment
import com.ib.asanarebelchallenge.ui.list.view.RepositoriesListFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector, AppNavigator {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, RepositoriesListFragment())
                .commit()
        }
    }

    override fun navigateToRepositoryDetailsFragment(repositoryOwner: String, repositoryName: String) {
        val detailsFragment = RepositoryDetailsFragment.newInstance(repositoryOwner, repositoryName)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, detailsFragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onBack() {
        supportFragmentManager.popBackStack()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
}
