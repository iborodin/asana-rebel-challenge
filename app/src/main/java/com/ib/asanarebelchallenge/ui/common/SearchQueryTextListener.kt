package com.ib.asanarebelchallenge.ui.common

import android.support.v7.widget.SearchView

class SearchQueryTextListener(
    private val submitSearch: (searchQuery: String) -> Unit
) : SearchView.OnQueryTextListener {

    override fun onQueryTextChange(newText: String?) = false

    override fun onQueryTextSubmit(query: String?): Boolean {
        return query?.let {
            submitSearch(it)
            true
        } ?: false
    }
}