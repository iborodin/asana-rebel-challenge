package com.ib.asanarebelchallenge.ui.list.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import com.ib.asanarebelchallenge.ui.ErrorListItem
import com.ib.asanarebelchallenge.ui.LoadingListItem
import com.ib.asanarebelchallenge.ui.RepositoryListItem
import com.ib.asanarebelchallenge.ui.RepositoryUiModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_error.view.*
import kotlinx.android.synthetic.main.list_item_repo.view.*
import android.support.v7.util.DiffUtil
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.ui.RepositoryUiDiffCallback
import com.ib.asanarebelchallenge.utils.inflateLayout

class RepositoriesAdapter(
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var repositories: List<RepositoryUiModel> = emptyList()

    fun updateItems(newItems: List<RepositoryUiModel>) {
        val diffResult = DiffUtil.calculateDiff(RepositoryUiDiffCallback(this.repositories, newItems))
        diffResult.dispatchUpdatesTo(this)
        repositories = newItems
    }

    override fun getItemCount() = repositories.size

    override fun getItemViewType(position: Int): Int = when (repositories[position]) {
        is RepositoryListItem -> VIEW_TYPE_ITEM
        is LoadingListItem -> VIEW_TYPE_LOADING
        is ErrorListItem -> VIEW_TYPE_ERROR
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        VIEW_TYPE_ITEM -> parent.inflateLayout(R.layout.list_item_repo)
            .let { RepositoryViewHolder(it, onClickListener) }
        VIEW_TYPE_LOADING -> parent.inflateLayout(R.layout.list_item_loading)
            .let(::LoadingViewHolder)
        VIEW_TYPE_ERROR -> parent.inflateLayout(R.layout.list_item_error)
            .let { ErrorViewHolder(it, onClickListener) }
        else -> error("Unknown viewType")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RepositoryViewHolder) {
            bindRepositoryItemViewHolder(holder, position)
        }
    }

    private fun bindRepositoryItemViewHolder(holder: RepositoryViewHolder, position: Int) {
        repositories[position]
            .let { it as? RepositoryListItem }
            ?.let { holder.bind(it.repository) }
    }

    interface OnClickListener {
        fun onRepositoryClick(repositoryOwner: String, repositoryName: String)
        fun onRetryClick()
    }

    class RepositoryViewHolder(
        itemView: View,
        private val onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(repository: GithubRepositoryModel) {
            with(itemView) {
                name.text = repository.name
                description.text = repository.description
                forksNumber.text = context.resources.getQuantityString(
                    R.plurals.number_of_forks,
                    repository.forksNumber,
                    repository.forksNumber
                )

                Picasso.get().load(repository.owner.avatarUrl).into(ownerAvatar)

                setOnClickListener { onClickListener.onRepositoryClick(repository.owner.login, repository.name) }
            }
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ErrorViewHolder(
        itemView: View,
        onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.retryButton.setOnClickListener { onClickListener.onRetryClick() }
        }
    }

    companion object {
        private const val VIEW_TYPE_ITEM = 1
        private const val VIEW_TYPE_LOADING = 2
        private const val VIEW_TYPE_ERROR = 3
    }
}