package com.ib.asanarebelchallenge.ui

import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel

sealed class RepositoryUiModel

data class RepositoryListItem(val repository: GithubRepositoryModel) : RepositoryUiModel()

object LoadingListItem : RepositoryUiModel()

object ErrorListItem : RepositoryUiModel()