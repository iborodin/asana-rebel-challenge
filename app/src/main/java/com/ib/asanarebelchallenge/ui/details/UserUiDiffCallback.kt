package com.ib.asanarebelchallenge.ui.details

import android.support.v7.util.DiffUtil

class UserUiDiffCallback(
    private val oldItems: List<UserUiModel>,
    private val newItems: List<UserUiModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return (oldItem is LoadingListItem && newItem is LoadingListItem) ||
                (oldItem is ErrorListItem && newItem is ErrorListItem) ||
                (oldItem is UserListItem && newItem is UserListItem &&
                        oldItem.user.login == newItem.user.login)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}