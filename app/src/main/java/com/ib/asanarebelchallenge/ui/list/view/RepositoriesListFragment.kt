package com.ib.asanarebelchallenge.ui.list.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.ui.AppNavigator
import com.ib.asanarebelchallenge.ui.RepositoryUiModel
import com.ib.asanarebelchallenge.ui.common.PaginationScrollListener
import com.ib.asanarebelchallenge.ui.common.SearchQueryTextListener
import com.ib.asanarebelchallenge.ui.list.presenter.RepositoriesListPresenter
import com.ib.asanarebelchallenge.ui.list.presenter.RepositoriesListView
import com.ib.asanarebelchallenge.utils.isVisible
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_repositories_list.*
import javax.inject.Inject

class RepositoriesListFragment : MvpAppCompatFragment(), RepositoriesListView, RepositoriesAdapter.OnClickListener {

    private lateinit var repositoriesAdapter: RepositoriesAdapter

    @Inject
    @InjectPresenter
    lateinit var presenter: RepositoriesListPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repositories_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSearchView()
        initList()
    }

    private fun initSearchView() {
        searchView.setOnClickListener { searchView.onActionViewExpanded() }
        searchView.setOnQueryTextListener(
            SearchQueryTextListener(presenter::requestRepos)
        )
    }

    private fun initList() {
        repositoriesAdapter = RepositoriesAdapter(this)
        val linearLayoutManager = LinearLayoutManager(context)
        repositoriesList.apply {
            adapter = repositoriesAdapter
            layoutManager = linearLayoutManager
            setHasFixedSize(true)

            addOnScrollListener(
                PaginationScrollListener(
                    linearLayoutManager,
                    presenter::requestNextPage
                )
            )
        }
    }

    override fun showEmptyResultMessage(show: Boolean) {
        emptyListMessage.isVisible = show
    }

    override fun showRepositoriesList(show: Boolean) {
        repositoriesList.isVisible = show
    }

    override fun updateRepositoriesListItems(repositories: List<RepositoryUiModel>) {
        repositoriesAdapter.updateItems(repositories)
    }

    override fun onRepositoryClick(repositoryOwner: String, repositoryName: String) {
        activity
            ?.let { it as? AppNavigator }
            ?.let { it.navigateToRepositoryDetailsFragment(repositoryOwner, repositoryName) }
    }

    override fun onRetryClick() {
        presenter.requestNextPage()
    }
}