package com.ib.asanarebelchallenge.ui

interface AppNavigator {

    fun navigateToRepositoryDetailsFragment(repositoryOwner: String, repositoryName: String)

    fun onBack()

}