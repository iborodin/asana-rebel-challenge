package com.ib.asanarebelchallenge.ui.list.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.ib.asanarebelchallenge.data.models.GithubRepositoryModel
import com.ib.asanarebelchallenge.domain.interactor.list.RepositoriesListInteractor
import com.ib.asanarebelchallenge.ui.ErrorListItem
import com.ib.asanarebelchallenge.ui.LoadingListItem
import com.ib.asanarebelchallenge.ui.RepositoryListItem
import com.ib.asanarebelchallenge.ui.RepositoryUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class RepositoriesListPresenter @Inject constructor(
    private val interactor: RepositoriesListInteractor
) : MvpPresenter<RepositoriesListView>() {

    private val subscriptions = CompositeDisposable()

    private val currentRepositories = mutableListOf<GithubRepositoryModel>()
    private var currentSearchQuery: String? = null
    private var hasNextPage: Boolean = true
    private var isLoading: Boolean = false

    fun requestRepos(query: String) {
        currentSearchQuery = query
        hasNextPage = true
        currentRepositories.clear()

        requestNextPage()
    }

    fun requestNextPage() {
        if (currentSearchQuery.isNullOrBlank() || isLoading || hasNextPage.not()) return

        interactor.searchRepositoriesByPage(currentSearchQuery!!)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                viewState.showEmptyResultMessage(false)
                showLoading()
                isLoading = true
            }
            .doFinally { isLoading = false }
            .doOnSuccess { hasNextPage = it.isNotEmpty() }
            .subscribe(
                { repositories ->
                    currentRepositories.addAll(repositories)

                    viewState.showEmptyResultMessage(currentRepositories.isEmpty())
                    viewState.showRepositoriesList(currentRepositories.isNotEmpty())

                    currentRepositories
                        .map { RepositoryListItem(it) }
                        .let(viewState::updateRepositoriesListItems)
                },
                {
                    Timber.e(it)
                    showError()
                }
            )
            .let(subscriptions::add)
    }

    private fun showLoading() {
        currentRepositories
            .map { RepositoryListItem(it) }
            .toMutableList<RepositoryUiModel>()
            .apply { add(LoadingListItem) }
            .let(viewState::updateRepositoriesListItems)
    }

    private fun showError() {
        currentRepositories
            .map { RepositoryListItem(it) }
            .toMutableList<RepositoryUiModel>()
            .apply { add(ErrorListItem) }
            .let(viewState::updateRepositoriesListItems)
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }
}