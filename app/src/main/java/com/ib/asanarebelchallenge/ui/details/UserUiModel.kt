package com.ib.asanarebelchallenge.ui.details

import com.ib.asanarebelchallenge.data.models.GithubUserModel

sealed class UserUiModel

data class UserListItem(val user: GithubUserModel) : UserUiModel()

object LoadingListItem : UserUiModel()

object ErrorListItem : UserUiModel()