package com.ib.asanarebelchallenge.ui.details.presenter

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ib.asanarebelchallenge.ui.details.UserUiModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface RepositoryDetailsView : MvpView {

    fun showTitle(title: String)

    fun showSubtitle(subtitle: String)

    fun showEmptyListMessage(show: Boolean)

    fun showUsersList(show: Boolean)

    fun updateUserListItems(users: List<UserUiModel>)

}