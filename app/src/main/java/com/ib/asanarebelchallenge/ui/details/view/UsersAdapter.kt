package com.ib.asanarebelchallenge.ui.details.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ib.asanarebelchallenge.R
import com.ib.asanarebelchallenge.data.models.GithubUserModel
import com.ib.asanarebelchallenge.ui.details.ErrorListItem
import com.ib.asanarebelchallenge.ui.details.LoadingListItem
import com.ib.asanarebelchallenge.ui.details.UserListItem
import com.ib.asanarebelchallenge.ui.details.UserUiDiffCallback
import com.ib.asanarebelchallenge.ui.details.UserUiModel
import com.ib.asanarebelchallenge.utils.inflateLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_error.view.*
import kotlinx.android.synthetic.main.list_item_user.view.*

class UsersAdapter(
    private val onClickListener: OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<UserUiModel> = emptyList()

    fun updateItems(newItems: List<UserUiModel>) {
        val diffResult = DiffUtil.calculateDiff(UserUiDiffCallback(this.items, newItems))
        diffResult.dispatchUpdatesTo(this)
        items = newItems
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is UserListItem -> VIEW_TYPE_ITEM
        is LoadingListItem -> VIEW_TYPE_LOADING
        is ErrorListItem -> VIEW_TYPE_ERROR
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_ITEM -> parent.inflateLayout(R.layout.list_item_user).let(::UserViewHolder)
            VIEW_TYPE_LOADING -> parent.inflateLayout(R.layout.list_item_loading).let(::LoadingViewHolder)
            VIEW_TYPE_ERROR -> parent.inflateLayout(R.layout.list_item_error)
                .let { ErrorViewHolder(it, onClickListener) }
            else -> error("Unknown viewType")
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is UserViewHolder) {
            bindUserViewHolder(viewHolder, position)
        }
    }

    private fun bindUserViewHolder(userViewHolder: UserViewHolder, position: Int) {
        items[position]
            .let { it as? UserListItem }
            ?.let { userViewHolder.bind(it.user) }
    }

    interface OnClickListener {
        fun onRetryClick()
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: GithubUserModel) {
            with(itemView) {
                login.text = user.login
                Picasso.get().load(user.avatarUrl).into(avatar)
            }
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ErrorViewHolder(
        itemView: View,
        onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.retryButton.setOnClickListener { onClickListener.onRetryClick() }
        }
    }

    companion object {
        private const val VIEW_TYPE_ITEM = 1
        private const val VIEW_TYPE_LOADING = 2
        private const val VIEW_TYPE_ERROR = 3
    }
}