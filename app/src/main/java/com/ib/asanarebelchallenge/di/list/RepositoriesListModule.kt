package com.ib.asanarebelchallenge.di.list

import com.ib.asanarebelchallenge.di.qualifiers.FragmentScope
import com.ib.asanarebelchallenge.domain.interactor.list.RepositoriesListInteractor
import com.ib.asanarebelchallenge.domain.interactor.list.RepositoriesListInteractorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoriesListModule {

    @FragmentScope
    @Binds
    abstract fun bindInteractor(interactorImpl: RepositoriesListInteractorImpl): RepositoriesListInteractor
}