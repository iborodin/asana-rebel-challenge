package com.ib.asanarebelchallenge.di.main

import com.ib.asanarebelchallenge.di.details.RepositoryDetailsModule
import com.ib.asanarebelchallenge.ui.list.view.RepositoriesListFragment
import com.ib.asanarebelchallenge.di.list.RepositoriesListModule
import com.ib.asanarebelchallenge.di.qualifiers.FragmentScope
import com.ib.asanarebelchallenge.ui.details.view.RepositoryDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [RepositoriesListModule::class])
    abstract fun repositoriesListFragment(): RepositoriesListFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [RepositoryDetailsModule::class])
    abstract fun repositoryDetailsFragment(): RepositoryDetailsFragment
}