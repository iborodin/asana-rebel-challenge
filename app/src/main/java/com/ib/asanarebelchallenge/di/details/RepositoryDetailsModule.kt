package com.ib.asanarebelchallenge.di.details

import com.ib.asanarebelchallenge.di.qualifiers.FragmentScope
import com.ib.asanarebelchallenge.domain.interactor.details.RepositoryDetailsInteractor
import com.ib.asanarebelchallenge.domain.interactor.details.RepositoryDetailsInteractorImpl
import com.ib.asanarebelchallenge.ui.details.view.RepositoryDetailsFragment
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RepositoryDetailsModule {

    @FragmentScope
    @Binds
    abstract fun bindInteractor(interactorImpl: RepositoryDetailsInteractorImpl): RepositoryDetailsInteractor

    @Module
    companion object {

        @Provides
        @JvmStatic
        @RepositoryOwner
        fun provideRepositoryOwner(fragment: RepositoryDetailsFragment): String = fragment.repositoryOwner

        @Provides
        @JvmStatic
        @RepositoryName
        fun provideRepositoryName(fragment: RepositoryDetailsFragment): String = fragment.repositoryName
    }
}