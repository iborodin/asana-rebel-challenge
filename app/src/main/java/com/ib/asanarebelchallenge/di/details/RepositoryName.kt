package com.ib.asanarebelchallenge.di.details

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class RepositoryName