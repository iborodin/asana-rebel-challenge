package com.ib.asanarebelchallenge.di.qualifiers

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope