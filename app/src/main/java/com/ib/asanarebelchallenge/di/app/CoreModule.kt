package com.ib.asanarebelchallenge.di.app

import android.app.Application
import android.content.Context
import com.ib.asanarebelchallenge.utils.resource.ResourceProvider
import com.ib.asanarebelchallenge.utils.resource.ResourceProviderImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class CoreModule {

    @Binds
    @Singleton
    abstract fun bindsContext(app: Application): Context

    @Binds
    @Singleton
    abstract fun bindsResourceProvider(resourceProvider: ResourceProviderImpl): ResourceProvider

}