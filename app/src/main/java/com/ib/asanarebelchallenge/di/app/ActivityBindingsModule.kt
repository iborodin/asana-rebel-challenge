package com.ib.asanarebelchallenge.di.app

import com.ib.asanarebelchallenge.ui.MainActivity
import com.ib.asanarebelchallenge.di.main.MainActivityModule
import com.ib.asanarebelchallenge.di.qualifiers.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingsModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity
}