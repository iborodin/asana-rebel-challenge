package com.ib.asanarebelchallenge.di.app

import android.app.Application
import com.ib.asanarebelchallenge.AsanaRebelChallengeApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingsModule::class,
        CoreModule::class,
        NetworkModule::class,
        DataModule::class
    ]
)
interface AppComponent {

    fun inject(app: AsanaRebelChallengeApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}