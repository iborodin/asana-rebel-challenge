package com.ib.asanarebelchallenge.di.app

import com.ib.asanarebelchallenge.domain.repositories.GithubRepositoriesRepository
import com.ib.asanarebelchallenge.domain.repositories.GithubRepositoriesRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun bindsRepository(repositoryImpl: GithubRepositoriesRepositoryImpl): GithubRepositoriesRepository

}