<h1>Asana Rebel Android Developer Test</h1>

The application allows search repositories and view repository details, using [GitHub v4 API](https://developer.github.com/v4/).

Follows up Clean Architecture principles and MVP as the presentation layer (using library Moxy).

<h1>Used libraries</h1>

- Android Support Libraries
- [Apollo GraphQL](https://github.com/apollographql/apollo-android) (GraphQL client)
- [Dagger2](https://github.com/google/dagger) (dependency injector)
- [RxJava2](https://github.com/ReactiveX/RxJava) (reactive programming extension)
- [Moxy](https://github.com/Arello-Mobile/Moxy) (MVP library)
- [Picasso](https://github.com/square/picasso) (Image downloading and caching)
- [Timber](https://github.com/JakeWharton/timber) (logger)
